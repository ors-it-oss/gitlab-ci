#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ Kaniko functions ------------------------#
kaniko_intel() {
  _header "KANIKO DIR"
  ls -alR /kaniko

  _header "KANIKO HELP"
  /kaniko/executor --help

  _header "USER RUN"
  ls -alR /run || true

  _header "WHOS ME"
  # shellcheck disable=SC2169
  echo $UID
  id -u
}


kaniko_authn() {
  export DOCKER_CONFIG=/run/user/$UID
  # shellcheck disable=SC2046
  _cri_authn $(_oci_reg_authn)
}


kaniko_build() {
  _header "Kaniko build of $DOCKERFILE"
  tags=$(_oci_tags)
  cat <<-INFO
    Destination tags:
    $tags
INFO

  [[ -n "$OCI_DEBUG" ]] && log_level=debug

  # https://github.com/GoogleContainerTools/kaniko/issues/1209#issuecomment-630013402
  sed -i 's|docker.io":|https://index.docker.io/v1/":|g' /run/user/$UID/config.json

  # shellcheck disable=SC2046
  eval $(cat <<-COMMAND
		exec /kaniko/executor \
			--verbosity "${log_level:-info}" \
			--reproducible --cache --cache-dir "$OCI_BUILD_DIR" --cache-repo "$REGISTRY_CACHE" \
			$(_env2cli build-arg OCI_BUILD_ARGS) \
			$(_cri_args_labels) \
			$(_cri_args_tags destination $tags) \
			--dockerfile "$DOCKERFILE" \
			--context "${OCI_BUILD_CTX:-.}"
	COMMAND
  )
}
