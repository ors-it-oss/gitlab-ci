#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ Jess Fraz' img functions ------------------------#
img_intel() {
  _header "IMG LS"
  img ls -s "${OCI_BUILD_DIR}"

  _header "IMG HELP"
  img --help || true
}


_img_authn() {
  while [[ $# -gt 0 ]]; do
    reg_server=$1; reg_user=$2; reg_pass=$3
    shift; shift; shift

#    if [[ "$reg_server" == "docker.io" ]]; then
#      # Resets it to default https://docker.io/v1/
#      # b0rks on auth otherwise
#      # https://github.com/genuinetools/img/issues/128
#      reg_server=
#    fi

    echo "$reg_pass" | img login -u "$reg_user" --password-stdin "$reg_server"
  done
}


img_authn() {
  # (v0.5.7) ignores $DOCKER_CONFIG
  HOME=/run/user/$(id -u)
  # shellcheck disable=SC2046
  _img_authn $(_oci_reg_authn)
}


img_build() {
  _header "j3ss' img build of $DOCKERFILE"
  tags=$(_oci_tags)
  cat <<-INFO
    HOME: $HOME
    DOCKER_CONFIG: $DOCKER_CONFIG
    Destination tags:
    $tags
INFO

  cd "$OCI_BUILD_CTX"

  # shellcheck disable=SC2046
  eval $(cat <<-COMMAND
  img build -s "${OCI_BUILD_DIR}" \
		$(_env2cli build-arg OCI_BUILD_ARGS) \
		$(_cri_args_labels) \
    $(_cri_args_tags tag $tags) \
    --file "$DOCKERFILE" \
    .
COMMAND
  )

  _header "Pushing tags..."
  for tag in $tags; do
#    if echo "$tag" | grep -q docker.io; then
#      tag="${tag#docker.io/}"
#    fi
    img push -s "${OCI_BUILD_DIR}" "$tag"
  done
}
