#@IgnoreInspection BashAddShebang
#shellcheck shell=ksh

REGISTRY_DOCKER=docker.io
REGISTRY_GITLAB=registry.gitlab.com
REGISTRY_GOOGLE_EU=eu.gcr.io
REGISTRY_GOOGLE=gcr.io
REGISTRY_QUAY=quay.io

#------------------------ include OCI functions ------------------------#
export OCI_BUILD_DIR="$CI_PROJECT_DIR/.build/oci"
if echo "$VERBOSE" | grep -q -e all -e oci; then
  export OCI_DEBUG=yesplz
fi

if [[ -z "$OCI_BUILD_CTX" ]]; then
  export OCI_BUILD_CTX=$(dirname "$DOCKERFILE")
  export DOCKERFILE=$(basename "$DOCKERFILE")
fi


##------------------------ authentication functions ------------------------##
_oci_reg_authn() {
	# Parse the builtin CI_REGISTRY and REGISTRY_ vars to $host $user $pass list
  for reg_user in CI_REGISTRY_USER $(_env_keys 'REGISTRY_.*_USER'); do
    if [[ "$reg_user" == "CI_REGISTRY_USER" ]]; then
    	reg_name=GitLab
      reg_user="$CI_REGISTRY_USER"
      reg_pass="$CI_REGISTRY_PASSWORD"
      reg_host="$CI_REGISTRY"
    else
			reg_name=$(echo "$reg_user"|cut -d_ -f2)
      reg_user="$(_env_get "$reg_user")"
      reg_pass="$(_env_get "REGISTRY_${reg_name}_PASSWORD")"
      reg_host="$(_env_get "REGISTRY_$reg_name")"
    fi

    [[ -n "$reg_host" ]] || { echo "No server found for container registry $reg_name!"; continue; }
# TODO: OCI_TAGS is empty when this is invoked through gitlab script[0]
#		echo "$OCI_TAGS" | grep -q "$reg_host" || { echo "Skipping unused ${reg_name} auth"; continue; }
    [[ "$reg_host" != "DISABLED" ]] || continue
    [[ -n "$reg_pass" ]] || { echo "No password given for container registry $reg_name!'"; continue; }

    echo -n "$reg_host $reg_user $reg_pass "
  done
}

_cri_authn_json() {
	# Take the parsed auth list & create auth json out of it
  cat <<-JSON
		{"auths": {
JSON
  while [[ $# -gt 0 ]]; do
    reg_server=$1; reg_user=$2; reg_pass=$3
    shift; shift; shift
    echo "Registering ${reg_user}@${reg_server}" >&2

    if [[ $# -gt 0 ]]; then
      comma=,
    else
      comma=
    fi
    cat <<-JSON
			"$reg_server": {"auth": "$(echo -n "${reg_user}:${reg_pass}" | base64 | tr -d '\n')" }$comma
JSON

  done
  cat <<-JSON
		}}
JSON
}

_cri_authn() {
  # Generates a Docker/whoever config.json with registry auths
  json="${AUTH_JSON:-${DOCKER_CONFIG:-${HOME}/.docker}/config.json}"
  json_dir="$(dirname "$json")"

  _header "Writing registry authentications to ${json}..."
  # shellcheck disable=SC2174
  [[ -d "$json_dir" ]] || mkdir -pm 700 "$json_dir"
  _cri_authn_json "$@" >"$json"

  if echo "$VERBOSE" | grep -iq trace; then
    echo 'Registry authentications result:'
    sed 's|auth": ".*"|auth": "[SEDSORED]"|g' "$json"
  fi
}


##------------------------ special arg handling ------------------------##
_cri_args_labels() {
  # Compile image labels
  # Labels from https://github.com/opencontainers/image-spec/blob/master/annotations.md
  # TODO: WTF is ref.name supposed to be anyway???

  prefix=org.opencontainers.image
  valid="authors created documentation description licenses revision source title url version vendor"
	OCI_AUTOLABELS=${OCI_AUTOLABELS:-all}
	[[ "$OCI_AUTOLABELS" == "all" ]] && OCI_AUTOLABELS="authors created revision source"

	for key in $OCI_AUTOLABELS; do
    case $key in
      authors) val="$GITLAB_USER_NAME ($GITLAB_USER_EMAIL)" ;;
      created) val=$PROJECT_TS_RFC3339 ;;
      revision) val="${CI_COMMIT_TAG:-${CI_COMMIT_SHORT_SHA:-$CI_COMMIT_SHA}}" ;;
      source) val="${PROJECT_URL:-${CI_PROJECT_URL}}" ;;
    esac

    [[ -n "$val" ]] || continue
    [[ "$key" == authors ]] && echo -n "--label \"maintainer=${val}\" "
    echo -n "--label \"${prefix}.${key}=${val}\" "
	done
 	env|awk -F= '/^PRODUCT_.*=/{ st=index($0,"="); a=tolower(substr($0,0,st-1)); gsub("product_","'$prefix'.", a); print "--label \"" a "=" substr($0,st+1) "\"" }'

	_env2cli label OCI_LABELS
}

_cri_args_tags() {
	param=$1; shift
	for tag in $@; do
		echo -n "--$param \"$tag\" "
	done
}

_oci_tags() {
	# ALl tags
  for reg_user in CI_REGISTRY_USER $(_env_keys 'REGISTRY_.*_USER'); do
    if [[ "$reg_user" == "CI_REGISTRY_USER" ]]; then
      reg_gitlab="$CI_REGISTRY_IMAGE"
    else
    	reg_name=$(echo "$reg_user"|sed 's|^REGISTRY_||g;s|_USER$||g')
      eval reg_$(_lower $reg_name)=\${REGISTRY_${reg_name}_IMAGE:-\$REGISTRY_$reg_name/\${REGISTRY_${reg_name}_SLUG:-\$REGISTRY_${reg_name}_USER}}
    fi
  done

	eval echo $(_env_vals "REGISTRY_TAGS(_.*)?")
}
