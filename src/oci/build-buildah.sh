#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ Buildah functions ------------------------#
buildah_intel() {
  _header "BUILDAH VERSION"
  buildah --version

  _header "BUILDAH HELP"
  buildah --help

  _header "BUILDAH BUD HELP"
  buildah bud --help
  
}


buildah_authn() {
  AUTH_JSON=/run/user/$UID/auth.json
  # shellcheck disable=SC2046
  _cri_authn $(_oci_reg_authn)
}


buildah_build() {
  _header "Buildah build of $DOCKERFILE"
#  [[ -n "$DOCKERFILE_CWD" ]] && cd "$DOCKERFILE_CWD"
  export TMPDIR="${OCI_BUILD_DIR}/tmp"
  [[ -d "${TMPDIR}" ]] || mkdir -pm 700 "${TMPDIR}"

  tags=$(_oci_tags)
  cat <<-INFO
    Destination tags:
    $tags
INFO

  # Prevent non-existent volume build errors
  for vol in $OCI_BUILD_VOLUMES; do
    dir=$(echo "$vol" | cut -d: -f1)
    [[ -d "$dir" ]] || { _header "OCI_BUILD_VOLUME $dir does not exist! Creating empty dir..."; mkdir -p "$dir"; }
  done

  # shellcheck disable=SC2046
  eval $(cat <<-COMMAND
  buildah ${OCI_DEBUG:+--log-level debug} \
    --root "${OCI_BUILD_DIR}/root" --runroot "/run/user/$(id -u)/containers" \
    bud --authfile "$AUTH_JSON" --isolation chroot --file "$DOCKERFILE" \
    --tag ${CI_PIPELINE_ID}:${CI_JOB_ID} \
		$(_env2cli build-arg OCI_BUILD_ARGS) \
    $(_cri_args_labels) \
    $(_env2cli volume OCI_BUILD_VOLUMES) \
  "${OCI_BUILD_CTX:-.}"
COMMAND
  )

  _header "Pushing tags..."
  for tag in $tags; do
    echo "Pushing ${tag}..."
    buildah ${OCI_DEBUG:+--log-level debug} \
      --root "${OCI_BUILD_DIR}/root" --runroot "/run/user/$(id -u)/containers" \
      push --authfile "$AUTH_JSON" "${CI_PIPELINE_ID}:${CI_JOB_ID}" "$tag"
  done
}
