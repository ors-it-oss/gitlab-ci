#!/bin/bash

# shellcheck disable=SC2034,SC2155
qa_golang(){
  _header "QA: GoLang CI"

  local report_name=golangci
  # shellcheck disable=SC2046,SC2086
  report_cmd(){
    golangci
  }
  local report_etl="$(cat <<-'REPORT'
#	.[0] | map([{
#	  type: "issue",
#	  fingerprint: "\(.code):\(.file):\(.line):\(.column)"|@base64,
#	  check_name: "shellcheck",
#	  description: "[SC\(.code)][\(.level)]: \(.message)",
#	  location: {path: .file, lines: {begin: .line, end: .endLine}},
#	  severity: .level
#	  }]) | add
	REPORT
  )"

  _qa_report

  #| jq -r '.[] | "\(.file):\(.line):\(.column): \(.level): \(.message) [SC\(.code)]"'

  error=$(cat /tmp/qaxit 2>/dev/null)
  return "${error:-0}"
}


qa_golang_intel() {
  _header "GOLANG $(go --version)"
  _header "GOLANGCI $(golangci --version)"

}
