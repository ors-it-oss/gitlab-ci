#@IgnoreInspection BashAddShebang
# shellcheck shell=ksh

#------------------------ include common functions ------------------------#
(echo "$VERBOSE" | grep -q -e "all" -e "shell") && set -x
set -e -o pipefail

PROJECT_TS_RFC3339=$(date -u '+%Y-%m-%dT%TZ')
VERSION_TS=$(date -u '+%y.%j.%H%M%S')
VERSION_TS_DAY=$(date -u '+%Y.%j')
VERSION_TS_MONTH=$(date -u '+%Y.%m')
VERSION_TS_WEEK=$(date -u '+%Y.%V')
sed_euroval='s/(^|[^\])€([{_a-zA-Z])/\1$\2/g;s/\\€/€/g'
[[ -n "$UID" ]] || { UID=$(id -u) && export UID; }
export PROJECT_TS_RFC3339 VERSION_TS VERSION_TS_DAY VERSION_TS_MONTH VERSION_TS_WEEK

_env_get(){
	# _env_get KEY default-value
	# Returns $KEY or default-value
  #
  # B0rked on busybox sh :(
  #val=$1
  #echo "${!val}"

  #res=$(env | sed -E -e "/^$1=/!d" -e 's|^[^=]+=||g')
  # shellcheck disable=SC2086
  eval 'echo "${'$1':-'$2'}"|sed -E "'$sed_euroval'"'
}

_env(){
	# _env_keys "FOO_(_.*)?_BAR"
	# Returns all variable names matching FOO_<something>_BAR

  #nope
  #echo "${!$1*}"
	env|sed -E -e "/^$1=/!d" -e "$sed_euroval"

}

_env_keys(){
	# _env_keys "FOO_(_.*)?_BAR"
	# Returns all variable names matching FOO_<something>_BAR

  #nope
  #echo "${!$1*}"
  env|awk -F= "/^$1=/{ print \$1 }"
}

_env_vals(){
	# _env_keys "FOO_(_.*)?_BAR"
	# Returns all variable values matching FOO_<something>_BAR

  #nope
  #echo "${!$1*}"
  #this also supports vars containing newlines
  for key in $(_env_keys "$@"); do
  	eval echo \$$key
	done | sed -E "$sed_euroval"
}

_header() {
  # Nicely formatted demarkation line. You're welcome ^^
  cat >&2 <<-TPL


		#------------------------ $1 ------------------------#
		#${2+ $2}

TPL
}

_env2param(){
	echo -n "--$cli_arg \""$val"\" "
}

_env2kwparam(){
	eval echo -n "\"--$cli_arg \\\"$val="\$$val"\\\" \""
}

_env2params(){
  # Transforms variable vars into params
  # _params_env myparam one two
  # Returns --myparam $one --myparam $two
  # _params_env myparam "one two" "ENV_ONE ENV_TWO"
  # Returns --myparam $one --myparam $two --myparam ENV_ONE=ENV_ONE --myparam ENV_TWO=ENV_TWO
  cli_arg=$1; class=$2; shift; shift; args="$@"
  echo -e "\nParameter '$cli_arg' list from '${args}'" >&2

	case $class in
		arg) etl=_env2param ;;
		kwarg) etl=_env2kwparam ;;
	esac

	for val in $(_env_vals "($(echo "$args"|sed 's/ /|/g'))"); do
		$etl
	done
}

_env2cli() {
	# Build a param list from a bunch of env vars
	# _env2cli build-arg OCI_BUILD
	# returns
	# 	all OCI_BUILD(_.*) variables as --param $val
	# 	all OCI_BUILD(_.*)_ENV variables as --param $k=$v
  arg=$1; var_base=$2

	args="$(_env_keys "${var_base}(_.*)?[^(_ENV)]")"
	[[ -n "$args" ]] && _env2params $arg arg $args
	kwargs="$(_env_keys "${var_base}(_.*)?_ENV")"
	[[ -n "$kwargs" ]] && _env2params $arg kwarg $kwargs
	return 0
}

_upper() {
  #B0rked on busybox sh :(
  #args="$*"
  #echo "${args^^}"
  echo "$@" | tr '[:lower:]' '[:upper:]'
}

_lower() {
  echo "$@" | tr '[:upper:]' '[:lower:]'
}

project_intel() {
  _header 'ADVANCED SHELL STUFF'
  echo "We uppered: $(_upper allcases)"
  echo "Get env HOME = $HOME = $(_env_get HOME)"

  _header "PROJECT ENVIRONMENT"
  env | sort

  _header "MOUNTS"
  mount

  _header "BUILDS DIR ($CI_BUILDS_DIR)"
  ls -al "$CI_BUILDS_DIR"

  _header "CURRENT DIR ($PWD) == PROJECT DIR ($CI_PROJECT_DIR)"
  ls -al
}
