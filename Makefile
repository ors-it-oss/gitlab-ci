include lib.mk
# sed -E -e '/^variables:/,/^$/!d' -e '/^[ ]*#/d' -e '/^  /!d' -e 's|^  ||g' -e 's|: | := |g' .gitlab-ci.yml

.PHONY: tpl qa test-oci test-lib

tpl:
	exec build/pre-commit.sh

test-oci:
	img=quay.io/buildah/stable:latest
	exec build/run.sh test-oci all

test-lib:
	img=quay.io/buildah/stable:latest
	exec build/run.sh test-lib argsNkwargs

qa:
	podman run --rm -it \
		--volume /home/theloeki/WS/OSS/salt-formulae:/src \
		--volume /home/theloeki/WS/OSS/gitlab-ci:/builds \
	registry.gitlab.com/ors-it-oss/oci:qa-python /bin/bash -c /builds/src/qa/.test.sh
