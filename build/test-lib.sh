#!/bin/sh
# Perform various tests on generic lib parts of the scripts

[[ -n "$CI" ]] || { set -a; . build/test.env; set +a; }

cd src
. ./lib.sh

###############################################################
###############################################################
###############################################################
all() {
	caps
	argsNkwargs
	env2params
}

caps() {
	_header "Capitalisations"
	_upper "ThIs WAsnT aLL UpPer"
	_lower "ThIs WAsnT aLL LoWeR"
}

argsNkwargs(){
	set -a
	JUSTAVAR="nuttin"
	JUSTANOTHERVAR="evenless"
	PARA_ONE_one="rood geel groen"
	PARA_ONE_two="blauw paars zwart"
	PARA_ONE_two_ENV=JUSTAVAR
	PARA_ONE_three_ENV=JUSTANOTHERVAR
	set +a

	_header "Testing mixing args 'n' kwargs..."
	_env2cli cliparam "PARA_ONE"
}


env2params() {
	set -a
	PARA_ONE_ONE="lalala"
	PARA_ONE_two="lololo"
	PARA_TWO="dingdong dangdang dongdong"
	PARA_TWO_nl=$(cat <<-ML
		oneone
		twotwo
		threethree
	ML
	)
	PARA_THREE_ARG_1="ko€{PARA_THREE_ONE_ENV}ek €PARA_THREE_two"
	PARA_THREE_ARG_2="ei"
	PARA_THREE_ONE="lalala"
	PARA_THREE_ONE_ENV="troeleie"
	PARA_THREE_ENV="troeleie"
	PARA_THREE_two="lololo"
	set +a

	_header "Testing env2params..."
	_env2params param arg PARA_ONE "PARA_TWO.*"

	_header "Testing env2kwparams..."
	_env2params kwparam kwarg PARA_ONE PARA_TWO

	_header "Testing env2cli..."
	_env2cli cliparam "PARA_THREE"

}

###############################################################
###############################################################
###############################################################

"$@"
