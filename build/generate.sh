#!/bin/bash
[[ -n "$DEBUG" ]] && set -x

#glinc=
gltpld=src
. src/lib.sh

generate_loop(){
	# with $gltpld as root
	# find all YAML templates and render 'em to $tdir
  tdir="${tdir:-.}"

  find "$gltpld" \( -name '*.yml' -o -name '*.yaml' \) -printf '%P\n' | while read -r tpl; do
    dst="$tdir/$(dirname "$tpl")"
    echo "Generating ${tpl} to $dst..."
    mkdir -p "$dst"
    (cd $gltpld; generate "$tpl") >"$tdir/$tpl"
    [[ -n "$git" ]] && git add "$tdir/$tpl"
  done || { if [[ "$?" -eq 1 ]]; then return 0; else return $?; fi ; }
}


generate() {
	# Take a template $1 in $gltpld root
	# find all *script.sh references
	# append a hidden anchor with actual script contents
  gltpl=$1
  (set +x; _header "GENERATE-INCLUDED FUNCTIONS" "Generated from src/! Change the scripts & re-generate." 2>&1 | sed -E '1,3{/^[[:space:]]*$/d}')

  # Find all YAML *refs & see if they exist as files
  sed -E '/ \*/!d;s|^[^*]+ \*||g' "$gltpl" | sort -u | (
    while read -r tpll; do
      [[ -s "$tpll" ]] || continue
      anchor=$(echo "$tpll" | sed -E 's|\.[^.]*$||g;s|[/.]|--|g')
      anchors="$anchors -e s|\*${tpll}([[:space:]]*)|*${anchor}\1|g"

      cat <<-YAML

				.${anchor}: &${anchor} |-
			YAML
      # Remove comments & indent
      #-e '/^[[:space:]]*#([@!]|[[:space:]]*shellcheck)/d' \
      sed -E \
        -e '/^[[:space:]]*(#[^-#].*)?$/d' \
        -e 's/^/  /' "$tpll"
    done

  (set +x; _header "END OF FUNCTIONS" 2>&1)

  if [[ -n "$anchors" ]]; then
    # shellcheck disable=SC2086
    sed -E $anchors "$gltpl"
  else
    cat "$gltpl"
  fi
  )
}

generate_loop
