#!/bin/sh
# Perform various tests on the OCI building part of the scripts

[[ -n "$CI" ]] || { set -a; . build/test.env; set +a; }

cd src
. ./lib.sh
. ./oci/oci.sh
. ./oci/build-buildah.sh
. ./oci/build-img.sh
. ./oci/build-kaniko.sh

###############################################################
###############################################################
###############################################################
all() {
	labels
	tags
	volumes
}

labels() {
	set -a
	PRODUCT_VENDOR="Droopy McScroug€"
	PRODUCT_REF="The Archit€ct"
	OCI_LABELS="canhaz=label"
	OCI_LABELS_extra="canhazzy=moarlabel"
	set +a

	_header "Label arguments"
	_cri_args_labels
}

tags() {
	set -a
	REGISTRY_DOCKER_USER=dockeruser
	REGISTRY_DOCKER_SLUG=mememe
	REGISTRY_QUAY_USER=quser
	REGISTRY_QUAY_IMAGE=quay.io/some/where
	REGISTRY_GOOGLE_EU_USER=guser
	REGISTRY_GOOGLE_USER=guser2
	ALPINE=something

	REGISTRY_TAGS="€reg_docker/toolbox:{latest,€VERSION_TS_DAY,alpine-$ALPINE} €reg_gitlab/toolbox:{latest,€VERSION_TS_DAY,borkine-$ALPINE} {€reg_google,€reg_google_eu}/toolbox:{latest,€VERSION_TS_DAY,borkine-$ALPINE}"
	set +a

	tags=$(_oci_tags)
	_header "TAGS"
	echo -e "$tags"

	_header "Resulting params"
	echo -e $(_cri_args_tags tag $tags)

}

volumes(){
	set -a
	OCI_BUILD_VOLUMES="a:a:ro b:b"
	set +a

	_header "Volumes"
	_env2cli volume OCI_BUILD_VOLUMES

}

###############################################################
###############################################################
###############################################################

"$@"
