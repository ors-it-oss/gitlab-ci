#!/bin/bash

#img=registry.gitlab.com/ors-it-oss/oci/centos:6
script="$1"
shift

img=quay.io/buildah/stable:latest

#img=r.j3ss.co/img
#--entrypoint "/bin/sh"

#img=gcr.io/kaniko-project/executor:debug
#		--entrypoint "/busybox/sh" \

podman run --rm -it \
		--tmpfs /run:exec --tmpfs /tmp:exec \
	  ${DEBUG:+--env DEBUG=$DEBUG} \
	  ${VERBOSE:+--env VERBOSE=$VERBOSE} \
	  --volume $PWD:/src:z \
	  --workdir /src \
		"$img" /src/build/${script}.sh "${@:-all}"
